# pdflatex build when using my1quest.sty

PDF_SRCS = $(subst .tex,,$(wildcard *.tex))
PDF_TGTS = $(filter-out my1quest,$(PDF_SRCS))
TGT_LIST = $(filter $(MAKECMDGOALS),$(PDF_TGTS))

ifneq ($(TGT_LIST),)
TGT_NAME = $(word 1,$(TGT_LIST))
else
TGT_NAME ?= my1quest
endif
PDF_MADE = $(addsuffix .pdf,$(PDF_SRCS))

LOG_FILE ?= >>/dev/null
FIND_IARG = $(foreach a,$1,$(if $(value $a),$a=$($a)))
FING_DBUG := $(call FIND_IARG,log)
ifneq ($(FING_DBUG),)
	DBG_FLAG = $(word 2,$(subst =, ,$(FING_DBUG)))
	ifeq ($(DBG_FLAG),1)
		LOG_FILE = >>$(TGT_NAME).l0g
	else ifeq ($(DBG_FLAG),0)
		LOG_FILE =
	endif
endif

.PHONY: dummy all clean sweep new $(PDF_SRCS)

.SECONDARY: *.pdf

dummy:
	@echo "Run 'make <tex>' OR 'make TGT_NAME=<tex>'"
	@echo "  <tex> = { $(PDF_SRCS) }"
	@echo "  <tgt> = { $(TGT_NAME) }"

all: $(TGT_NAME).pdf

$(TGT_NAME): $(TGT_NAME).pdf
	@echo "  <lst> = { $(PDF_SRCS) }"
	@echo "  <tgt> = { $(TGT_NAME) }"

define build_rule
$(1): LOG_FILE = >>/dev/null
$(1): $(1).pdf
endef
CHK_TGTS = $(filter-out $(TGT_NAME),$(PDF_SRCS))
$(foreach tgt, $(CHK_TGTS) my1quest,$(eval $(call build_rule,$(tgt))))

%.pdf: %.tex
	pdflatex $(subst .tex,,$<) $(LOG_FILE)
	pdflatex $(subst .tex,,$<) $(LOG_FILE)

clean:
	-rm -f  *.out *.aux *.log *.l0g

sweep: clean
	-rm -f $(PDF_MADE)

new: sweep all
