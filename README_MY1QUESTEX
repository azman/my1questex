----------
my1questex
----------

My personal LaTeX style formatting for UniMAP final exam papers. Specifically,
using pdflatex to directly create PDF from LaTeX source.

There are 4 files in this repo:
- README       : this file
- makefile     : i use this in my workflow
- my1quest.sty : THE LateX style file (tested using TexLive 2020)
- my1quest.tex : example using my1quest.sty

*Disclaimer: I am sharing this as it is. Feel free to use it at your own risk.

Commands provided by my1quest.sty:

- \questdone      : ends (sub-)question numbering
- \questinit      : starts main numbering (a),(b)... MUST end with \questdone
- \questsubs      : starts subs numbering (i),(ii).. MUST end with \questdone
- \questmark      : show marks (right-aligned) for a (sub-)question

- \questends      : go to next page (start next on new page)
- \questpagecover : print cover page
- \questexamcover : print page information (exams)
- \questtestcover : print student information (tests)
- \questdoinst**  : insert instruction in cover page
                  : (** => {AB, XX, ZZ})
- \questshowSectA : for \questdoinstAB (instructions for section A)
- \questshowSectB : for \questdoinstAB (instructions for section B)
- \questpagestart : start question page AFTER a cover page
- \questdonefinal : put end of questions marker

Checkout m1quest.tex to see examples on using more commands and special
environment(s) provided by my1quest.sty.

*Note: To list all those, the following can be used,
$ cat my1quest.sty | grep newcommand
$ cat my1quest.sty | grep newenvironment
